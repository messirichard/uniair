<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'uniair');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '^|Fo,Z&^~:^r!zQ|)`}Pz&TZdIQe37aMdua@an0(9{J+&jqI8U9}X#Q_w!SAX3R*');
define('SECURE_AUTH_KEY',  'tRyXmp?}IQ0p CoBb>/MF1~/$1w;3Hk3{VX{Z]s,/T(GVVw<,k^mj!PeHczfv.!F');
define('LOGGED_IN_KEY',    'p9l5,)0Zd}CRPw$xQmWhwNso)Q?$IvJWAmTq~?Hv08A-sYo`,ortj[}93w#9aLsh');
define('NONCE_KEY',        '9.lVOMrd q#;yl`!,q,/Bj:A)P>O%`UaPSS|`IpL1@H g]s3y/<:N0Cs-z:4eali');
define('AUTH_SALT',        '0TE8k~BA%5=pW2_WY{ce,tmNSE3i;G8{ `QRkzs7Qz ]>NeQ_!R;l5dy9D{JEEv/');
define('SECURE_AUTH_SALT', '5Xy+J)Y?0*YNRz,86MqA%v]7/&yL=;U4eEa8BoaL<}F=k*|8j[8|q5>^uVdD.&Q#');
define('LOGGED_IN_SALT',   '{!AfoYDt}&I,5ccg0Nd2!5dMO)fAD644{.wfRd@Kp,9VYs?MMU@Nn:E,0{.HA6@4');
define('NONCE_SALT',       '^Dd4nwxdFTtMm|Ld>aTPqyW]Xn6v*Fk36y+/YU{{G[Zl(=2X6EHq*ECATO:Fwq2l');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
